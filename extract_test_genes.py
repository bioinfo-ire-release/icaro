import firebrowse
import json
import urllib
import tarfile
import csv
import sys
from numpy import genfromtxt
import argparse
import pandas
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np
import os

import pdb; 

analyses = firebrowse.Analyses()
cohorts = firebrowse.Metadata().Cohorts(format="tsv")

cohorts = cohorts.split("\n")

# for each cohort, extract top 10 deleted genes
for c in cohorts:
  if(c == "" or "cohort" in c):
    continue
  c = c.split("\t")
  cohort = c[0]
  description = c[1]
  #print cohort, description
  #print "extracting deleted genes...:"
  # found it in the help( firebrowse.Analyses())
  # TODO check whether it would be better to test without tresholds and breakpoints
  del_lines = analyses.CopyNumberGenesDeleted(cohort=cohort, page_size="10", format="tsv")
  
  del_lines = del_lines.split("\n")
  
  for d in del_lines: 
    if(d == "" or "gene" in d):
      continue
    d = d.split("\t")
    gene = d[0]
    # TODO clean output
    # exclude weird small nuclear genes (SNORS) and ribosomal ones (RN something)
    # exclude genes with double gene symbol
    
    print gene, cohort
   