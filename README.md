# ICAro
Inferring gene signature from Cancer copy number Aberrations

The intended purpose of this package is to extract a gene signature from public cancer genomic data (e.g., the TCGA), by focusing on focal deletions and RNA-seq. The tool is based on Broad Institute's firebrowse Python Package.

For the analysis of a single gene on a single TCGA dataset, run the icaro.sh script. As input it requires the gene name, the dataset name, the output folder name, a 0-1 parameter for chosing if random forest approach has to be applied or not (it takes several minutes). Optionally, as final parameter the user can pass a file name which contains mutations list. If a sample contains one if these mutations, such sample is included in the positive group.

example:
sh icaro.sh PTEN COAD output_pten_coad 1


For the analysis of many genes in many datasets, create a txt file with a pair of gene and TCGA dataset names, separated by one space, for each row. (e.g. driver_genes.tsv)

Run the icaro_authomatic.sh script, which requires the list of genes and datasets, the output folder name and 0-1 parameter for the selection of the random forest approach.

example:
sh icaro_authomatic.sh top_driver_genes.tsv prova_automatic 1

# Additional Partitioning of the Patient Sets

Some users would want to include other selection criteria for both activated/inactivated set, by exploiting, for instance, the large amount of clinical and metadata annotation included in TCGA samples.
Unfortunately, most of those are cancer or tissue-specific. Our advice is to customize the main icaro_beta.py workflow with the use of the firebrowse.Samples.Clinical API in order to fetch annotation and filter patients according to them. 
