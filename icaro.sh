#/bin/sh
#$1 gene
#$2 dataset
#$3 output folder
#$4 email  ##
#$4 apply random forest (0: no; 1: yes)
#$5 inactivating mutation list file

#cd icaro

FILEPATH="./$3/all_genes_"$1"_"$2".tsv"

echo "executing python script..."



input_type=1	   #0: both mRNA and miRNA; 1: only mRNA; 2: only miRNA
sample_selection=1 #1: CNV and mutation list; 2: only mutation list

if [ "$#" -eq "4" ]
then
  python -W ignore icaro_beta.py --gene $1 --dataset $2 --output $3 --input_type $input_type --sample_selection $sample_selection > icaro_out
else
  python -W ignore icaro_beta.py --gene $1 --dataset $2 --output $3 --inac_mut $5 --input_type $input_type --sample_selection $sample_selection > icaro_out
fi

if [ $? -eq "1" ]
then
    echo "Analysis interrupted"
    icaro_log=$(cat icaro_out)
    #echo "Dear User, \n\nthis is an automated message from the ICAro system. Unfortunately, the analysis couldn't be completed. This is the log message:\n\n$icaro_log\n\nKind Regards,\nthe ICAro team"  | mutt  -s  "ICAro output $1 $2" -b matteo.pallocca@gmail.com -d 1 -- $4
else	
	idx_file="$3/mRNA_index_columns_$1_$2.txt"
	if [ -f $idx_file ]; then
		data_dir="data"
		gzipped_file="${data_dir}/mRNA_$2.txt.gz"
		reduced_file="$3/mRNA_reduced_$1_$2.txt"

		idx=$(cat $idx_file)
		zgrep -v 'raw_count' $gzipped_file | cut -f $idx > $reduced_file
		rm $idx_file
	fi
	
        idx_file="$3/mRNA_index_columns_$1_$2_normalized_data.txt"
	if [ -f $idx_file ]; then
		data_dir="data"
		gzipped_file="${data_dir}/mRNA_$2.txt.gz"
		reduced_file="$3/mRNA_reduced_$1_$2_normalized_data.txt"

		idx=$(cat $idx_file)
		zgrep -v 'raw_count' $gzipped_file | cut -f $idx > $reduced_file
		rm $idx_file
	fi

	idx_file="$3/miRNA_index_columns_$1_$2.txt"
	if [ -f $idx_file ]; then
		data_dir="data"
		gzipped_file="${data_dir}/miRNA_$2.txt.gz"
		reduced_file="$3/miRNA_reduced_$1_$2.txt"

		idx=$(cat $idx_file)
		zgrep -v 'read_count' $gzipped_file | cut -f $idx > $reduced_file

		rm $idx_file
	fi
	
	idx_file="$3/miRNA_index_columns_$1_$2_normalized_data.txt"
	if [ -f $idx_file ]; then
		data_dir="data"
		gzipped_file="${data_dir}/miRNA_$2.txt.gz"
		reduced_file="$3/miRNA_reduced_$1_$2_normalized_data.txt"

		idx=$(cat $idx_file)
		zgrep -v 'read_count' $gzipped_file | cut -f $idx > $reduced_file

		rm $idx_file
	fi
	echo "executing R script..."
	Rscript icaro.R $1 $2 $3 $input_type 1 $4
	#echo "Dear User, \n\nthis is an automated message from the ICAro system. You will find attached your analysis output.\n\nKind Regards,\nthe ICAro team"  | mutt -a $FILEPATH -s  "ICAro output $1 $2" -b matteo.pallocca@gmail.com -d 1 -- $4
fi


#chown www-data:www-data "data/mutations_$3_$4.tsv"
